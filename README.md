# Login Screen WordPress

> A simple WordPress plugin to customize your login screen.

**Maintainer** : Tajid Yakub <tajid.yakub@gmail.com>

| Maintainer | Tajid Yakub <tajid.yakub@gmail.com>
|--|--|
| Documentation | view `docs` folder
