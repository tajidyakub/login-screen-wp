<?php
/**
 * Core class for the plugin
 */

class LoginScreen {
    protected $slug;
    protected $version;
    protected $loader;

    public function __construct() {
        // Populate properties
        $this->slug     = LOGIN_SCREEN_SLUG;
        $this->version  = LOGIN_SCREEN_VERSION;
        // Load Dependencies
        $this->loaddeps();
        $this->set_locale();
        $this->load_assets();
        $this->login_screen();
    }

    private function loaddeps() {
        require_once plugin_dir_path(dirname(__FILE__)) . 'incs/class-login-screen-assets.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'incs/class-login-screen-i18n.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'incs/class-login-screen-loader.php';
        $this->loader = new LoginScreenLoader();
    }

    private function set_locale() {
        $i18n = new LoginScreenI18n();
        $this->loader->add_action( 'plugins_loaded', $i18n, 'load_plugin_textdomain' );
    }

    private function load_assets() {
        $utils = new LoginScreenAssets();
        $this->loader->add_action('login_enqueue_scripts', $utils, 'enqueues');
    }

    private function login_screen() {
        $this->loader->add_filter('login_header_url', $this, 'login_screen_url');
        $this->loader->add_filter('login_header_title', $this, 'login_screen_url_title');
    }

    private function login_screen_url() {
        return home_url();
    }

    private function login_screen_url_title() {
        return __('Back to Home ', 'login-screen') . get_bloginfo('name');
    }

    public function run() {
        $this->loader->run();
    }

    public function get_plugin_name() {
        return $this->plugin_name;
    }

    public function get_loader() {
        return $this->loader;
    }

    public function get_version() {
        return $this->version;
    }
}
