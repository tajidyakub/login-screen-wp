<?php
/**
 * Plugin's entry point.
 *
 * @package login-screen
 * @version 1.0.0
 * @link    https://bitbucket.org/tajidyakub/customwp-plugin
 *
 * @login-screen
 * Plugin Name:       Login Screen
 * Plugin URI:        https://bitbucket.org/tajidyakub/login-screen-wp.git
 * Description:       A simple plugin to customize your WordPress login screen.
 * Version:           1.0.0
 * Author:            Tajid Yakub
 * Author URI:        https://tajidyakub.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       login-screen
 * Domain Path:       /languages
 */

if ( ! defined( 'WPINC' ) ) {
    die;
}

define('LOGIN_SCREEN_SLUG', 'login-screen');
define('LOGIN_SCREEN_VERSION', '1.0.0');

// require init class
require_once plugin_dir_path( __FILE__ ) . 'incs/class-login-screen-init.php';

function login_screen_activate() {
    LoginScreenInit::activate();
}

function login_screen_deactivate() {
    LoginScreenInit::deactivate();
}
// activation and deactivateion hook register
register_activation_hook( __FILE__, 'login_screen_activate' );
register_deactivation_hook( __FILE__, 'login_screen_deactivate' );

// require core class
require_once plugin_dir_path( __FILE__ ) . 'incs/class-login-screen.php';

// require core class
function login_screen_execute() {
    $login_screen = new LoginScreen();
    $login_screen->run();
}

login_screen_execute();
