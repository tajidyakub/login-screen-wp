/**
 * Grunt tasks definition file.
 *
 * Adjust configurations in the given config file
 * and rename accodingly tobe required from this file.
 */
const config = require("./_config/config");

module.exports = function(grunt) {
  grunt.initConfig({
    // Deployment through rsync, will synchronize localPath with remotePath
    rsync: {
      options: {
        args: ["--verbose"],
        exclude: config.sync.exclude,
        recursive: true
      },
      remote: {
        options: {
          src: config.sync.localPath,
          dest: config.sync.remotePath,
          delete: true
        }
      }
    },
    compress: {
      plugins: {
        options: {
          archive: "release/" + config.slug + "-" + config.version + ".zip"
        },
        expand: true,
        cwd: "src/plugins/",
        src: ["**/*"],
        dest: "/"
      }
    }
  });

  // Next one would load plugins
  grunt.loadNpmTasks("grunt-rsync");
  grunt.loadNpmTasks("grunt-contrib-compress");
  grunt.registerTask("deploy", "Deploy to remote web server", ["rsync:remote"]);
  grunt.registerTask("release", "Compress files in src/plugins/", ["compress:plugins"]);
};
